"""
WSGI config for Jagu project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os,sys, site
from getpass import getuser
local_user = getuser()

# Tell wsgi to add the Python site-packages to its path.

if (local_user == 'JAGU') or (local_user == 'Andres'):
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Jagu.settings")

    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()
else:
    site.addsitedir('/home/jaguven/.virtualenvs/admin/lib/python2.7/site-packages')

    os.environ['DJANGO_SETTINGS_MODULE'] = 'Jagu.settings'

    activate_this = os.path.expanduser("~/.virtualenvs/admin/bin/activate_this.py")
    execfile(activate_this, dict(__file__=activate_this))

    # Calculate the path based on the location of the WSGI script
    project = '/home/jaguven/webapps/admin/admin/'
    workspace = os.path.dirname(project)
    sys.path.append(workspace)
    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()
