import sys
import os
from getpass import getuser

ADMINS = (
     ('Andres Gomes', 'agomes@jagu.com.ve'),
)
MANAGERS = ADMINS

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
ROOT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

local_user = getuser()

STATIC_URL = '/static/'

BASE_URL = "dev.jagu.com.ve"

Cust_id = "28215523"

if (local_user == 'JAGU') or (local_user == 'Andres'):
    DB_USER = 'root'
    DB_PASS = 'root'
    HOST = 'http://localhost:8000'
    MEDIA_ROOT = os.path.join(ROOT_PATH, 'static/media')
    Root_static = os.path.join(ROOT_PATH, 'static')


else:
    DB_USER = 'jaguven'
    DB_PASS = 'morfeo88'
    HOST = 'dev.jagu.com.ve'
    MEDIA_ROOT = '/home/jaguven/webapps/admin_static/media/'
    Root_static = '/home/jaguven/webapps/admin_static/'

STATICFILES_DIRS = (
    Root_static,
)
# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'jagu',                      # Or path to database file if using sqlite3.
        'USER': DB_USER,                      # Not used with sqlite3.
        'PASSWORD': DB_PASS,                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '4(em3y^v=%(&7m=csji#mh4x^v&k36xj)=m8osb6es4l@%yybr'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'Home',
)
TEMPLATE_DIRS = (os.path.join(BASE_DIR, "templates"),)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'Jagu.urls'

WSGI_APPLICATION = 'Jagu.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Caracas'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

