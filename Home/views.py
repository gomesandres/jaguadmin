from __future__ import division
# -*- coding: utf-8 -*-
__author__ = 'Andres'

from django.http import HttpResponseRedirect,HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.db.models import Q
from Home.models import *
from Home.forms import *
from easy_pdf.rendering import render_to_pdf_response
import urllib, json
from django.contrib import auth

def registro(request,msg=''):
    """
    Vista para crea un usuario nuevo
    """
    data = {}
    loginForm = LoginForm()
    data["msg"] = msg
    try:
        user = Usuario.objects.get(pk=request.user.id)
        data["user"] = user
    except Exception:
        pass
    if request.method == 'POST':
        registrationForm = RegistrationForm(request.POST,request.FILES)
        if registrationForm.is_valid():
            user = Usuario.objects.crear_usuario(registrationForm.cleaned_data['nickname'],
                                                 registrationForm.cleaned_data['email'],
                                                 registrationForm.cleaned_data['password'],
                                                 registrationForm.cleaned_data['nombre'],
                                                 registrationForm.cleaned_data['apellido']
                                                 )
            if user is not None:
                usuario = Usuario.objects.get(pk=user.id)
                usuario.save();
            return home(request,"Registrado con Exito!")
        else:
            data["registrationForm"] = registrationForm
            data["errorReg"] = registrationForm.errors
            return render_to_response('registro.html',data,
                                      context_instance=RequestContext(request))
    else:
        registrationForm = RegistrationForm()
        data["registrationForm"] = registrationForm
        return render_to_response('registro.html',data,
                                  context_instance=RequestContext(request))



def login(request,msg=''):
    data = {}
    data["msg"] = msg
    loginForm = LoginForm()
    data["loginForm"]=loginForm
    try:
        usuario = Usuario.objects.get(pk=request.user.id)
        return home(request,msg)
    except Exception:
        if request.method == 'POST':
            loginForm = LoginForm(request.POST)
            data["loginForm"]=loginForm
            if loginForm.is_valid():
                usr = loginForm.cleaned_data['username']
                pwd = loginForm.cleaned_data['password']
                user = auth.authenticate(username=usr, password=pwd)
                if user is not None and user.is_active:
                    auth.login(request, user)
                    try:
                        usuario = Usuario.objects.get(user_ptr_id=user.id)
                        return home(request,"")
                    except Usuario.DoesNotExist:
                        data["msg"]= "Usuario no existe"
                else:
                    data["msg"]= "Nombre de usuario y/o contrasena invalidos"
            else:
                data["msg"]= "Debe completar todos los campos"
        return render_to_response('login.html',data,
                context_instance=RequestContext(request))

def loged(request,data,msg=''):
    try:
        usuario = Usuario.objects.get(pk=request.user.id)
        data["usuario"] = usuario
        data["monedaForm"] = monedaForm()
        data["transaccionesForm"] = transaccionesForm()
        try:
            if(request.session['msg']):
                data["msg"] = request.session['msg']
                request.session['msg'] = None
        except:
            pass

        return True
    except Exception:
        if request.method == 'POST':
            loginForm = LoginForm(request.POST)
            data["loginForm"]=loginForm
            if loginForm.is_valid():
                usr = loginForm.cleaned_data['username']
                pwd = loginForm.cleaned_data['password']
                user = auth.authenticate(username=usr, password=pwd)
                if user is not None and user.is_active:
                    auth.login(request, user)
                    try:
                        usuario = Usuario.objects.get(user_ptr_id=user.id)
                        data["usuario"] = usuario
                        data["monedaForm"] = monedaForm()
                        data["transaccionesForm"] = transaccionesForm()
                        try:
                            if(request.session['msg']):
                                data["msg"] = request.session['msg']
                                request.session['msg'] = None
                        except:
                            pass
                        return True
                    except Usuario.DoesNotExist:
                        data["msg"]= "Usuario no existe"
                else:
                    data["msg"]= "Nombre de usuario y/o contrasena invalidos"
            else:
                data["msg"]= "Debe completar todos los campos"
        loginForm = LoginForm()
        data["loginForm"]=loginForm
        return False


def home(request,msg=''):
    data = {}
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))

    data["articulos"] = Articulo.objects.all()

    url = "https://s3.amazonaws.com/dolartoday/data.json"
    response = urllib.urlopen(url)
    response = json.loads(unicode(response.read(),"ISO-8859-1"))
    data["simadi"] = response["USD"]["sicad2"]
    data["negro"] = response["USD"]["dolartoday"]
    Caps = []
    Capital_total = 0
    for M in DIVISAS:
        try:
            capital = Capitales.objects.filter(divisa=M[0]).order_by('-fecha')[0]
            if(capital.divisa == 'DF' or capital.divisa == 'DE' or capital.divisa == 'DG'):
                Capital_total = Capital_total + (capital.cantidad * data["simadi"])
            else:
                Capital_total = Capital_total + capital.cantidad
            capital.cantidad = "{:,}".format(capital.cantidad)
            Caps.append(capital)
        except:
            pass
    count = 0
    try:
        arts = Articulo.objects.all()
        for a in arts:
            total = a.precio * a.cantidad
            if(total >= 0):
                Capital_total += total
            else:
                msg = "Hay un error en el articulo " + a.__str__()
    except:
        pass

    try:
        pedidos = Pedido.objects.filter(pagado = False)
        count = pedidos.__len__()
        data["pedidos"] = pedidos
    except:
        pass
    if(count > 5):
        count = 5
    data["msg"] = msg
    data["TT"] = "{:,}".format(GananciasTipoMes('TT'))
    data["DE"] = "{:,}".format(GananciasTipoMes('DE'))
    data["Jagu"] = "{:,}".format(GananciasTipoMes('Jagu'))
    data["transacciones"] = Transaccion.objects.all().order_by('-id')[:10-count]
    data["Caps"] = Caps
    data["Capital_total"] = "{:,}".format(Capital_total)

    return render_to_response('index.html',data,
            context_instance=RequestContext(request))



def Factura(request,id=''):
    data = {}
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    obj = Transaccion.objects.get(pk = int(id))
    try:
        data["Fecha"] = obj.fecha.strftime("%d/%m/%Y")
        data["FacturaN"] = 11583 + obj.id
        data["FacturarA"] = obj.cliente.__str__()
        data["Identificacion"] = obj.cliente.Cedula
        data["Direccion"] = obj.cliente.Direccion
        articulos = []
        arts = LineaVenta.objects.filter(transaccion = obj)
        for a in arts:
            articulo = {}
            articulo["cantidad"] = a.cantidad
            articulo["nombre"] = a.articulo.__str__()
            articulo["precio"] = "{:,}".format(a.articulo.precio)
            total = a.cantidad * a.articulo.precio
            articulo["precioTotal"] = "{:,}".format(total)
            articulos.append(articulo)

        data["articulos"] = articulos
        artlen = articulos.__len__()
        rest = []
        for i in range(25-artlen):
            rest.append("x")
        data["rest"] = rest
        iva = obj.impuesto
        data["subtotal"] = "{:,}".format(obj.monto - iva)
        data["IVA"] = "{:,}".format(iva)
        data["total"] = "{:,}".format(obj.monto)

        return render_to_pdf_response(request, 'admin/factura.html', data)
    except:
        request.session['msg'] = "Esta transaccion no posee factura"
        return HttpResponseRedirect(request.META['HTTP_REFERER'])


def Facturar(request,id=''):
    data = {}
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    obj = Transaccion.objects.get(pk = int(id))
    msg = obj.Facturar()

    if(msg == True):
        return Factura(request,obj.id)
    else:
        request.session['msg'] = msg
        return HttpResponseRedirect(request.META['HTTP_REFERER'])

def Registrar(request,id=''):
    data = {}
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    obj = Transaccion.objects.get(pk = int(id))
    lineas = 0
    if(not obj.registrada):
        try:
            div  = LineaDivisa.objects.get(transaccion = obj)
            lineas = div.__len__()
            obj.tipo = 'O'
            obj.descripcion = "Compra de " +  str(div.cantidad) + " " + div.get_divisa_compra_display()
            obj.impuesto = 0
            obj.registrada = True
            obj.save()
            IntercambioCapital(obj.monto,obj.divisa,div.cantidad,div.divisa_compra)
            request.session['msg'] = "Registrado exitosamente"
        except:
            try:
                if(lineas == 0):
                    obj.impuesto = 0
                    obj.registrada = True
                    obj.save()
                    ActualizarCapital(obj.tipo,obj.monto,obj.divisa)
                    request.session['msg'] = "Registrado exitosamente"
            except:
                request.session['msg'] = "No puedes registrar esta transaccion"
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    else:
        request.session['msg'] = "Esta transaccion ya fue registrada"
        return HttpResponseRedirect(request.META['HTTP_REFERER'])


#def UpdateArticulos(request,msg=''):
#    data = {}
#    params = {}
#    params["access_token"] = meli.access_token
#    response = meli.get("/users/"+Cust_id+"/items/search",params)
#    if(response.status_code == 400):
#        return loging(request)
#    items = response.json()
#    articulos = parse_and_save(items)
#    return home(request)


def editar(request,object,id=0,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    Form = None
    obj = None
    guardar = True
    if request.method == 'POST':



        if(object == "Transaccion"):
            if(int(id) > 0):
                obj = Transaccion.objects.get(pk=int(id))
                Form = transaccionesForm(request.POST,request.FILES,instance=obj)
            else:
                Form = transaccionesForm(request.POST,request.FILES)
        elif(object == "Moneda"):
            if(int(id) > 0):
                obj = Moneda.objects.get(pk=int(id))
                Form = monedaForm(request.POST,request.FILES,instance=obj)
            else:
                Form = monedaForm(request.POST,request.FILES)
        elif(object == "Categoria"):
            if(int(id) > 0):
                obj = Categoria.objects.get(pk = int(id))
                Form = categoriaForm(request.POST,request.FILES,instance=obj)
            else:
                Form = categoriaForm(request.POST,request.FILES)
        elif(object == "Marca"):
            if(int(id) > 0):
                obj = Marca.objects.get(pk = int(id))
                Form = marcaForm(request.POST,request.FILES,instance=obj)
            else:
                Form = marcaForm(request.POST,request.FILES)
        elif(object == "Articulo"):
            if(int(id) > 0):
                obj = Articulo.objects.get(pk = int(id))
                Form = articuloForm(request.POST,request.FILES,instance=obj)
            else:
                Form = articuloForm(request.POST,request.FILES)
        elif(object == "Pedido"):
            if(int(id) > 0):
                obj = Pedido.objects.get(pk = int(id))
                Form = pedidoForm(request.POST,request.FILES,instance=obj)
            else:
                Form = pedidoForm(request.POST,request.FILES)
        elif(object == "LineaPedido"):
            if(int(id) > 0):
                obj = LineaPedido.objects.get(pk = int(id))
                Form = pedidoLineaForm(request.POST,request.FILES,instance=obj)
            else:
                Form = pedidoLineaForm(request.POST,request.FILES)
        elif(object == "Cliente"):
            if(int(id) > 0):
                obj = Cliente.objects.get(pk = int(id))
                Form = clienteForm(request.POST,request.FILES,instance=obj)
            else:
                Form = clienteForm(request.POST,request.FILES)
        if(object == "Venta"):
            if(int(id) > 0):
                obj = Transaccion.objects.get(pk=int(id))
                Form = transaccionVentaForm(request.POST,request.FILES,instance=obj)
            else:
                Form = transaccionVentaForm(request.POST,request.FILES)
        if(object == "CompraDivisa"):
            if(int(id) > 0):
                obj = Transaccion.objects.get(pk=int(id))
                Form = transaccionCompraDivisa(request.POST,request.FILES,instance=obj)
            else:
                Form = transaccionCompraDivisa(request.POST,request.FILES)
        elif(object == "LineaVenta"):
            if(int(id) > 0):
                obj = LineaVenta.objects.get(pk = int(id))
                Form = lineaVentaForm(request.POST,request.FILES,instance=obj)
            else:
                Form = lineaVentaForm(request.POST,request.FILES)
        elif(object == "LineaDivisa"):
            if(int(id) > 0):
                obj = LineaDivisa.objects.get(pk = int(id))
                Form = lineaDivisaForm(request.POST,request.FILES,instance=obj)
            else:
                Form = lineaDivisaForm(request.POST,request.FILES)


        if(Form.is_valid()):
            obj = Form.save(commit=False)

            if(object == "LineaPedido"):
                ped = Pedido.objects.get(pk= request.session['pedidoId'])
                try:
                    linea = LineaPedido.objects.get(pedido = ped , articulo=obj.articulo)
                    linea.cantidad += obj.cantidad
                    linea.save()
                    guardar = False
                except:
                    pass
                obj.pedido = ped
                ped.ActualizarPeso()
            elif(object == "LineaVenta" or object == "LineaDivisa"):
                tra = Transaccion.objects.get(pk= request.session['transaccionId'])
                obj.transaccion = tra
                try:
                    linea = LineaVenta.objects.get(transaccion = tra , articulo=obj.articulo)
                    linea.cantidad += obj.cantidad
                    linea.save()
                    guardar = False
                except:
                    pass

                try:
                    linea = LineaDivisa.objects.get(transaccion = tra , divisa_compra=obj.divisa_compra)
                    linea.cantidad += obj.cantidad
                    linea.save()
                    guardar = False
                except:
                    pass

            elif(object == "Pedido" and obj.id == None):
                obj.CalcularFechaRecepcion()


            if(guardar):
                obj.save()


            if(object == "LineaVenta"):
                articulos = LineaVenta.objects.filter(transaccion = obj.transaccion)
                monto = 0.0
                for art in articulos:
                    monto += art.articulo.precio * art.cantidad
                obj.transaccion.monto = monto
                obj.transaccion.impuesto = (monto/100)*12
                obj.transaccion.save()
            if(object == "LineaPedido"):
                obj.pedido.ActualizarPeso()
            if(object == "Pedido"):
                return HttpResponseRedirect("/EditarPedido/"+str(obj.id))
            if(object == "Transaccion"):
                return HttpResponseRedirect("/EditarTransaccion/"+str(obj.id))
            if(object == "Venta"):
                return HttpResponseRedirect("/EditarVenta/"+str(obj.id))
            if(object == "CompraDivisa"):
                return HttpResponseRedirect("/EditarCompra/"+str(obj.id))

            request.session['msg'] = "Guardado con exito"
        else:
            request.session['msg'] = Form.errors
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    else:
        return HttpResponseRedirect(request.META['HTTP_REFERER'])


def get_form(request,object,id=0,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    Form = None
    obj = None
    if(object == "Moneda"):
        if(int(id) > 0):
            obj = Moneda.objects.get(pk = int(id))
            Form = monedaForm(instance=obj)
        else:
            Form = monedaForm()
        data["titulo"] = "Moneda"
    elif(object == "Transaccion"):
        if(int(id) > 0):
            obj = Transaccion.objects.get(pk = int(id))
            Form = transaccionesForm(instance=obj)
        else:
            Form = transaccionesForm()
        data["titulo"] = "Transaccion"
    elif(object == "Categoria"):
        if(int(id) > 0):
            obj = Categoria.objects.get(pk = int(id))
            Form = categoriaForm(instance=obj)
        else:
            Form = categoriaForm()
        data["titulo"] = "Categoria"
    elif(object == "Marca"):
        if(int(id) > 0):
            obj = Marca.objects.get(pk = int(id))
            Form = marcaForm(instance=obj)
        else:
            Form = marcaForm()
        data["titulo"] = "Marca"
    elif(object == "Articulo"):
        if(int(id) > 0):
            obj = Articulo.objects.get(pk = int(id))
            Form = articuloForm(instance=obj)
        else:
            Form = articuloForm()
        data["titulo"] = "Articulo"
    elif(object == "Pedido"):
        if(int(id) > 0):
            obj = Pedido.objects.get(pk = int(id))
            Form = pedidoForm(instance=obj)
        else:
            Form = pedidoForm()
        data["titulo"] = "Pedido"
    elif(object == "LineaPedido"):
        if(int(id) > 0):
            obj = LineaPedido.objects.get(pk = int(id))
            Form = pedidoLineaForm(instance=obj)
        else:
            Form = pedidoLineaForm()
        data["titulo"] = "LineaPedido"
    elif(object == "LineaVenta"):
        if(int(id) > 0):
            obj = LineaVenta.objects.get(pk = int(id))
            Form = lineaVentaForm(instance=obj)
        else:
            Form = lineaVentaForm()
        data["titulo"] = "LineaVenta"
    elif(object == "LineaDivisa"):
        if(int(id) > 0):
            obj = LineaDivisa.objects.get(pk = int(id))
            Form = lineaDivisaForm(instance=obj)
        else:
            Form = lineaDivisaForm()
        data["titulo"] = "LineaDivisa"
    elif(object == "Venta"):
        if(int(id) > 0):
            obj = Transaccion.objects.get(pk = int(id))
            Form = transaccionVentaForm(instance=obj)
        else:
            Form = transaccionVentaForm()
        data["titulo"] = "Venta"
    elif(object == "CompraDivisa"):
        if(int(id) > 0):
            obj = Transaccion.objects.get(pk = int(id))
            Form = transaccionCompraDivisa(instance=obj)
        else:
            Form = transaccionCompraDivisa()
        data["titulo"] = "CompraDivisa"
    elif(object == "Cliente"):
        if(int(id) > 0):
            obj = Cliente.objects.get(pk = int(id))
            Form = clienteForm(instance=obj)
        else:
            Form = clienteForm()
        data["titulo"] = "Cliente"

    data["obj"] = obj
    lista = list(Form)
    q = lista.__len__()
    q = int(q/2)
    data["Form1"]=lista[0:q]
    data["Form2"]=lista[q:]

    return render_to_response('tools/form.html',data,
                              context_instance=RequestContext(request))


def eliminar(request,object,id,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    obj = None
    if(object == "Moneda"):
        obj = Moneda.objects.get(pk=int(id))
    elif(object == "Transaccion"):
        obj = Transaccion.objects.get(pk=int(id))
        if(obj.registrada):
            request.session['msg'] = "No puedes eliminar una transaccion que fue facturada."
            return HttpResponseRedirect(request.META['HTTP_REFERER'])
    elif(object == "Categoria"):
        obj = Categoria.objects.get(pk = int(id))
    elif(object == "Marca"):
        obj = Marca.objects.get(pk = int(id))
    elif(object == "Articulo"):
        obj = Articulo.objects.get(pk = int(id))
    elif(object == "Pedido"):
        obj = Pedido.objects.get(pk = int(id))
    elif(object == "LineaPedido"):
        obj = LineaPedido.objects.get(pk = int(id))
    elif(object == "LineaVenta"):
        obj = LineaVenta.objects.get(pk = int(id))
    elif(object == "LineaDivisa"):
        obj = LineaDivisa.objects.get(pk = int(id))
    elif(object == "Cliente"):
        obj = Cliente.objects.get(pk = int(id))
    obj.delete()
    request.session['msg'] = "Eliminado con Exito!"
    return HttpResponseRedirect(request.META['HTTP_REFERER'])



def Categorias(request,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    categorias = Categoria.objects.all()
    data["categorias"] = categorias
    return render_to_response('admin/categorias.html',data,
                                  context_instance=RequestContext(request))

def Marcas(request,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    marcas = Marca.objects.all()
    data["marcas"] = marcas
    return render_to_response('admin/marcas.html',data,
                                  context_instance=RequestContext(request))


def Pedidos(request,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    pedidos = Pedido.objects.all()
    data["pedidos"] = pedidos
    return render_to_response('admin/pedidos.html',data,
                                  context_instance=RequestContext(request))

def EditarPedido(request,id,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    obj = Pedido.objects.get(pk = int(id))
    arts = LineaPedido.objects.filter(pedido=obj)
    Form = pedidoForm(instance=obj)
    artForm = pedidoLineaForm()
    request.session['pedidoId'] = int(id)
    data["Form"] = Form
    data["artForm"] = artForm
    data["arts"] = arts
    data["obj"] = obj
    return render_to_response('admin/edit_pedido.html',data,
                              context_instance=RequestContext(request))


def NuevaVenta(request,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    obj = Transaccion()
    obj.tipo = 'I'
    obj.descripcion = "Venta sin Facturar"
    obj.save()
    return HttpResponseRedirect("/EditarVenta/"+str(obj.id))



def NuevaCompra(request,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    obj = Transaccion()
    obj.tipo = 'E'
    obj.descripcion = "Compra de divisas sin registrar"
    obj.save()
    return HttpResponseRedirect("/EditarCompra/"+str(obj.id))



def NuevoPedido(request,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    obj = Pedido()
    obj.save()
    return HttpResponseRedirect("/EditarPedido/"+str(obj.id))


def EditarVenta(request,id,msg=''):
    data = {}
    data["msg"] = msg
    loged(request,data)
    arts = None
    obj = None
    request.session['transaccionId'] = int(id)
    if(int(id) != 0):
        obj = Transaccion.objects.get(pk = int(id))
        arts = LineaVenta.objects.filter(transaccion=obj)
        Form = transaccionVentaForm(instance=obj)
    else:
        Form = transaccionVentaForm()

    artForm = lineaVentaForm()
    data["Form"] = Form
    data["artForm"] = artForm
    data["arts"] = arts
    data["obj"] = obj
    return render_to_response('admin/edit_venta.html',data,
                              context_instance=RequestContext(request))


def Registrar_pedido(request,id,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))

    obj = Pedido.objects.get(pk = int(id))
    if(obj.costoReal == 0):
        request.session['msg'] = "Debe registrar el costo Pagado por el pedido"
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    obj.pagado = True
    lineas = LineaPedido.objects.filter(pedido=obj)
    for L in lineas:
        art = L.articulo
        capital = ObtenerCapital(obj.divisa)
        cstProm = capital.costo_promedio
        costo = ((((obj.costoReal/obj.pesoTotal)*art.peso)/obj.costoTotal)*art.costo)*L.cantidad
        costo += (art.precio/100)*7
        costo += (cstProm * art.costo)
        costo = (int(costo*100))/100
        art.costoTotal = costo
        art.cantidad+=L.cantidad
        art.save()
        ActualizarCapital('E',obj.costoTotal,obj.divisa)
        ActualizarCapital('E',obj.costoReal,'BE')
    obj.save()
    request.session['msg'] = "Registrado con Exito!"
    return HttpResponseRedirect(request.META['HTTP_REFERER'])

def EditarCompra(request,id,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    arts = None
    obj = None
    request.session['transaccionId'] = int(id)
    if(int(id) != 0):
        obj = Transaccion.objects.get(pk = int(id))
        arts = LineaDivisa.objects.filter(transaccion=obj)
        Form = transaccionCompraDivisa(instance=obj)
    else:
        Form = transaccionCompraDivisa()

    artForm = lineaDivisaForm()
    data["Form"] = Form
    data["artForm"] = artForm
    data["arts"] = arts
    data["obj"] = obj
    return render_to_response('admin/edit_compra.html',data,
                              context_instance=RequestContext(request))



def EditarTransaccion(request,id,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    arts = None
    obj = None
    lD = None
    lV = None
    request.session['transaccionId'] = int(id)
    if(int(id) != 0):
        obj = Transaccion.objects.get(pk = int(id))
        lD = LineaDivisa.objects.filter(transaccion=obj)
        lV = LineaVenta.objects.filter(transaccion=obj)
        Form = transaccionesForm(instance=obj)
    else:
        Form = transaccionesForm()

    artForm = lineaDivisaForm()
    data["Form"] = Form
    data["artForm"] = artForm
    data["lD"] = lD
    data["lV"] = lV
    data["obj"] = obj
    return render_to_response('admin/edit_transaccion.html',data,
                              context_instance=RequestContext(request))


def Monedas(request,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    monedas = Moneda.objects.all().order_by('divisa')
    data["monedas"] = monedas
    return render_to_response('admin/monedas.html',data,
            context_instance=RequestContext(request))


def Transacciones(request,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    transacciones = Transaccion.objects.all()
    data["transacciones"] = transacciones
    return render_to_response('admin/transacciones.html',data,
            context_instance=RequestContext(request))

def Clientes(request,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    if(request.GET.get("query")):
        clientes = Cliente.objects.filter(Q(Cedula__icontains = request.GET["query"]) |
                                          Q(Nombre__icontains = request.GET["query"]) |
                                          Q(Apellido__icontains = request.GET["query"]))
    else:
        clientes = Cliente.objects.all()
    data["clientes"] = clientes
    return render_to_response('admin/clientes.html',data,
            context_instance=RequestContext(request))

def Articulos(request,msg=''):
    data = {}
    data["msg"] = msg
    if(not loged(request,data)):
        return render_to_response('login.html',data,
            context_instance=RequestContext(request))
    if(request.GET.get("query")):
        arts = Articulo.objects.filter(Q(modelo__icontains=request.GET["query"]) |
                                       Q(marca__titulo__icontains=request.GET["query"]) |
                                       Q(categoria__titulo__icontains=request.GET["query"]))
    else:
        arts = Articulo.objects.all()
    for a in arts:
        ganancia = a.precio - a.costoTotal
        ganancia = (ganancia*100)/a.precio
        ganancia = (int(ganancia*100)/100)
        a.ganancia = ganancia
    data["articulos"] = arts
    return render_to_response('admin/articulos.html',data,
                                  context_instance=RequestContext(request))
