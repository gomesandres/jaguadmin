# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Home', '0007_pedido_divisa'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ganancias',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha_creacion', models.DateField(auto_now=True)),
                ('gananciaTT', models.FloatField(default=0.0, blank=True)),
                ('gananciaDE', models.FloatField(default=0.0, blank=True)),
                ('gananciaJagu', models.FloatField(default=0.0, blank=True)),
                ('transaccion', models.ForeignKey(related_name='Ganancia', to='Home.Transaccion')),
            ],
            options={
                'db_table': 'ganancias',
                'verbose_name': 'ganancias',
            },
        ),
    ]
