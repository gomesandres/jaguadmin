# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Home', '0004_articulo_costoenvio'),
    ]

    operations = [
        migrations.AddField(
            model_name='articulo',
            name='pesoVolumetrico',
            field=models.FloatField(default=0.0, null=True, blank=True),
        ),
    ]
