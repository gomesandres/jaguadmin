# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Home', '0008_ganancias'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ganancias',
            name='transaccion',
            field=models.ForeignKey(related_name='Ganancia', to='Home.Transaccion', unique=True),
        ),
    ]
