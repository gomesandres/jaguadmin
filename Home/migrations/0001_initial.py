# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Art_ML',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('warranty', models.CharField(max_length=500, null=True)),
                ('original_price', models.FloatField(default=0.0, null=True, blank=True)),
                ('subtitle', models.CharField(max_length=150, null=True)),
                ('geolocation', models.CharField(max_length=150, null=True)),
                ('listing_source', models.CharField(max_length=150, null=True)),
                ('site_id', models.CharField(max_length=150, null=True)),
                ('buying_mode', models.CharField(max_length=150, null=True)),
                ('seller_custom_field', models.CharField(max_length=150, null=True)),
                ('currency_id', models.CharField(max_length=150, null=True)),
                ('descriptions', models.CharField(max_length=150, null=True)),
                ('automatic_relist', models.CharField(max_length=150, null=True)),
                ('last_updated', models.CharField(max_length=150, null=True)),
                ('ML_id', models.CharField(max_length=150, null=True)),
                ('secure_thumbnail', models.CharField(max_length=150, null=True)),
                ('title', models.CharField(max_length=150, null=True)),
                ('pictures', models.CharField(max_length=2000, null=True)),
                ('coverage_areas', models.CharField(max_length=150, null=True)),
                ('stop_time', models.CharField(max_length=150, null=True)),
                ('price', models.FloatField(default=0.0, null=True, blank=True)),
                ('seller_contact', models.CharField(max_length=150, null=True)),
                ('location', models.CharField(max_length=150, null=True)),
                ('status', models.CharField(max_length=150, null=True)),
                ('parent_item_id', models.CharField(max_length=150, null=True)),
                ('tags', models.CharField(max_length=150, null=True)),
                ('differential_pricing', models.CharField(max_length=150, null=True)),
                ('start_time', models.CharField(max_length=150, null=True)),
                ('permalink', models.CharField(max_length=150, null=True)),
                ('official_store_id', models.CharField(max_length=150, null=True)),
                ('date_created', models.CharField(max_length=150, null=True)),
                ('accepts_mercadopago', models.CharField(max_length=150, null=True)),
                ('available_quantity', models.IntegerField(default=0, null=True, blank=True)),
                ('condition', models.CharField(max_length=150, null=True)),
                ('sub_status', models.CharField(max_length=150, null=True)),
                ('seller_id', models.IntegerField(default=0, null=True, blank=True)),
                ('catalog_product_id', models.CharField(max_length=150, null=True)),
                ('seller_address', models.CharField(max_length=750, null=True)),
                ('video_id', models.CharField(max_length=150, null=True)),
                ('variations', models.CharField(max_length=150, null=True)),
                ('non_mercado_pago_payment_methods', models.CharField(max_length=150, null=True)),
                ('shipping', models.CharField(max_length=150, null=True)),
                ('thumbnail', models.CharField(max_length=150, null=True)),
                ('end_time', models.CharField(max_length=150, null=True)),
                ('sold_quantity', models.IntegerField(default=0, null=True, blank=True)),
                ('listing_type_id', models.CharField(max_length=150, null=True)),
                ('attributes', models.CharField(max_length=150, null=True)),
                ('category_id', models.CharField(max_length=150, null=True)),
                ('initial_quantity', models.IntegerField(default=0, null=True, blank=True)),
                ('deal_ids', models.CharField(max_length=150, null=True)),
                ('base_price', models.FloatField(default=0.0, null=True, blank=True)),
                ('fecha', models.DateTimeField(default=django.utils.timezone.now, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Articulo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('modelo', models.CharField(max_length=150, db_index=True)),
                ('costo', models.FloatField(default=0.0, null=True, blank=True)),
                ('peso', models.FloatField(default=0.0, null=True, blank=True)),
                ('precio', models.FloatField(default=0.0, null=True, blank=True)),
                ('cantidad', models.IntegerField(default=0, null=True, blank=True)),
                ('umbral_compra', models.IntegerField(default=0, null=True, blank=True)),
            ],
            options={
                'db_table': 'articulo',
                'verbose_name': 'articulo',
            },
        ),
        migrations.CreateModel(
            name='Capitales',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha', models.DateTimeField(auto_now=True)),
                ('cantidad', models.FloatField(default=0.0)),
                ('divisa', models.CharField(default=b'BE', max_length=50, choices=[(b'BF', b'Bolivares en Efectivo'), (b'BE', b'Bolivares Electronicos'), (b'DF', b'Dolares en Efectivo'), (b'DE', b'Dolares Electronicos'), (b'EF', b'Eutos en Efectivo'), (b'EE', b'Euros Electronicos'), (b'DG', b'Dolares en GiftCard')])),
                ('costo_promedio', models.FloatField(default=0.0, blank=True)),
            ],
            options={
                'db_table': 'capitales',
                'verbose_name': 'capitales',
            },
        ),
        migrations.CreateModel(
            name='Caracteristicas',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, db_index=True)),
                ('descripcion', models.CharField(max_length=200)),
                ('articulo', models.ForeignKey(related_name='caracteristicas', to='Home.Articulo')),
            ],
            options={
                'db_table': 'caracteristicas',
                'verbose_name': 'caracteristicas',
            },
        ),
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, db_index=True)),
                ('hoja', models.BooleanField(default=True)),
                ('padre', models.ForeignKey(related_name='Hijo', blank=True, to='Home.Categoria', null=True)),
            ],
            options={
                'db_table': 'Categoria',
                'verbose_name': 'Categoria',
            },
        ),
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombre', models.CharField(max_length=50)),
                ('Apellido', models.CharField(max_length=50)),
                ('Cedula', models.IntegerField(default=0)),
                ('Direccion', models.CharField(max_length=150)),
                ('Telefono', models.CharField(max_length=50)),
                ('Correo', models.CharField(max_length=50)),
                ('Pseudonimo_MercadoLibre', models.CharField(max_length=50)),
                ('fecha_creacion', models.DateField(auto_now=True)),
            ],
            options={
                'db_table': 'cliente',
                'verbose_name': 'cliente',
            },
        ),
        migrations.CreateModel(
            name='LineaDivisa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('divisa_compra', models.CharField(default=b'DE', max_length=50, choices=[(b'BF', b'Bolivares en Efectivo'), (b'BE', b'Bolivares Electronicos'), (b'DF', b'Dolares en Efectivo'), (b'DE', b'Dolares Electronicos'), (b'EF', b'Eutos en Efectivo'), (b'EE', b'Euros Electronicos'), (b'DG', b'Dolares en GiftCard')])),
                ('cantidad', models.IntegerField(default=0)),
            ],
            options={
                'db_table': 'lineadivisa',
                'verbose_name': 'lineadivisa',
            },
        ),
        migrations.CreateModel(
            name='LineaPedido',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cantidad', models.IntegerField(default=0)),
                ('articulo', models.ForeignKey(related_name='pedido', to='Home.Articulo')),
            ],
            options={
                'db_table': 'lineapedido',
                'verbose_name': 'lineapedido',
            },
        ),
        migrations.CreateModel(
            name='LineaVenta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cantidad', models.IntegerField(default=0)),
                ('articulo', models.ForeignKey(related_name='articuloLineaVenta', to='Home.Articulo')),
            ],
            options={
                'db_table': 'lineaventa',
                'verbose_name': 'lineaventa',
            },
        ),
        migrations.CreateModel(
            name='Marca',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, db_index=True)),
            ],
            options={
                'db_table': 'Marca',
                'verbose_name': 'Marca',
            },
        ),
        migrations.CreateModel(
            name='Moneda',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('divisa', models.CharField(default=b'BE', max_length=50, choices=[(b'BF', b'Bolivares en Efectivo'), (b'BE', b'Bolivares Electronicos'), (b'DF', b'Dolares en Efectivo'), (b'DE', b'Dolares Electronicos'), (b'EF', b'Eutos en Efectivo'), (b'EE', b'Euros Electronicos'), (b'DG', b'Dolares en GiftCard')])),
                ('costo_promedio', models.FloatField(default=0.0, blank=True)),
            ],
            options={
                'db_table': 'moneda',
                'verbose_name': 'moneda',
            },
        ),
        migrations.CreateModel(
            name='Pedido',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pagado', models.BooleanField(default=False)),
                ('costoTotal', models.FloatField(default=0.0, blank=True)),
                ('fechaCompra', models.DateField(default=datetime.datetime.today)),
                ('fechaEsRecepcion', models.DateField(default=datetime.datetime.today, blank=True)),
                ('fechaRecepcion', models.DateField(default=datetime.datetime.today, blank=True)),
                ('pesoTotal', models.FloatField(default=0.0, blank=True)),
                ('costoEstimado', models.FloatField(default=0.0, blank=True)),
                ('costoReal', models.FloatField(default=0.0, blank=True)),
            ],
            options={
                'db_table': 'pedido',
                'verbose_name': 'pedido',
            },
        ),
        migrations.CreateModel(
            name='Transaccion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha', models.DateTimeField(auto_now=True)),
                ('tipo', models.CharField(max_length=8, choices=[(b'I', b'Ingreso'), (b'E', b'Egreso'), (b'O', b'Otro')])),
                ('monto', models.FloatField(default=0.0)),
                ('impuesto', models.FloatField(default=0.0)),
                ('divisa', models.CharField(default=b'BE', max_length=50, choices=[(b'BF', b'Bolivares en Efectivo'), (b'BE', b'Bolivares Electronicos'), (b'DF', b'Dolares en Efectivo'), (b'DE', b'Dolares Electronicos'), (b'EF', b'Eutos en Efectivo'), (b'EE', b'Euros Electronicos'), (b'DG', b'Dolares en GiftCard')])),
                ('descripcion', models.CharField(default=b'', max_length=150, blank=True)),
                ('registrada', models.BooleanField(default=False)),
                ('cliente', models.ForeignKey(blank=True, to='Home.Cliente', null=True)),
            ],
            options={
                'db_table': 'transaccion',
                'verbose_name': 'transaccion',
            },
        ),
        migrations.AddField(
            model_name='lineaventa',
            name='transaccion',
            field=models.ForeignKey(related_name='lineaVenta', to='Home.Transaccion'),
        ),
        migrations.AddField(
            model_name='lineapedido',
            name='pedido',
            field=models.ForeignKey(related_name='lineas', to='Home.Pedido'),
        ),
        migrations.AddField(
            model_name='lineadivisa',
            name='transaccion',
            field=models.ForeignKey(related_name='lineaDivisa', to='Home.Transaccion'),
        ),
        migrations.AddField(
            model_name='articulo',
            name='categoria',
            field=models.ForeignKey(blank=True, to='Home.Categoria', null=True),
        ),
        migrations.AddField(
            model_name='articulo',
            name='marca',
            field=models.ForeignKey(blank=True, to='Home.Marca', null=True),
        ),
    ]
