# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Home', '0002_auto_20150409_1920'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cliente',
            name='Cedula',
            field=models.CharField(max_length=50),
        ),
    ]
