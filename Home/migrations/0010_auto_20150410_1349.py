# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Home', '0009_auto_20150410_1348'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ganancias',
            name='transaccion',
            field=models.OneToOneField(related_name='Ganancia', to='Home.Transaccion'),
        ),
    ]
