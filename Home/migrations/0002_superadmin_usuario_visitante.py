# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('Home', '0001_squashed_0010_auto_20150410_1349'),
    ]

    operations = [
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'usuario',
                'verbose_name': 'usuario',
            },
            bases=('auth.user',),
        ),
        migrations.CreateModel(
            name='Visitante',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=50)),
                ('email', models.CharField(unique=True, max_length=50, db_index=True)),
                ('fecha', models.DateTimeField(default=django.utils.timezone.now, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Superadmin',
            fields=[
                ('usuario_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='Home.Usuario')),
            ],
            options={
                'db_table': 'superadmin',
                'verbose_name': 'superadmin',
            },
            bases=('Home.usuario',),
        ),
    ]
