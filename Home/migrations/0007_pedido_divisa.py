# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Home', '0006_auto_20150410_0759'),
    ]

    operations = [
        migrations.AddField(
            model_name='pedido',
            name='divisa',
            field=models.CharField(default=b'DE', max_length=50, choices=[(b'BF', b'Bolivares en Efectivo'), (b'BE', b'Bolivares Electronicos'), (b'DF', b'Dolares en Efectivo'), (b'DE', b'Dolares Electronicos'), (b'EF', b'Eutos en Efectivo'), (b'EE', b'Euros Electronicos'), (b'DG', b'Dolares en GiftCard')]),
        ),
    ]
