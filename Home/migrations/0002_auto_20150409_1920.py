# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('Home', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='articulo',
            name='costoTotal',
            field=models.FloatField(default=0.0, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='pedido',
            name='fechaCompra',
            field=models.DateField(default=datetime.date.today),
        ),
        migrations.AlterField(
            model_name='pedido',
            name='fechaEsRecepcion',
            field=models.DateField(default=datetime.date.today, blank=True),
        ),
        migrations.AlterField(
            model_name='pedido',
            name='fechaRecepcion',
            field=models.DateField(default=datetime.date.today, blank=True),
        ),
    ]
