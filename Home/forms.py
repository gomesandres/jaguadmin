# -*- coding: utf-8 -*-
import re
from django import forms
from django.forms import ModelForm
from models import *
from django.forms import TextInput

from Jagu import settings

class LoginForm(forms.Form):
    """
    Formulario para login
    """
    username = forms.CharField(label=u'Nombre de usuario', widget=forms.TextInput(attrs={'placeholder': 'Usuario'}),
                               required=True)
    password = forms.CharField(label=u'Contraseña',
                               widget=forms.PasswordInput(render_value=False, attrs={'placeholder': 'Password'}),
                               required=True)


class RegistrationForm(forms.Form):
    """
    Formulario para el registro de usuarios
    """
    nickname = forms.CharField(max_length=50, label=u'Nombre de usuario', widget=forms.TextInput(attrs={'class': ''}),
                                required=True)
    nombre = forms.CharField(max_length=50, label=u'Nombre', widget=forms.TextInput(attrs={'class': ''}),
                                required=True)
    apellido = forms.CharField(max_length=50, label=u'Apellido', widget=forms.TextInput(attrs={'class': ''}),
                                required=True)
    email = forms.EmailField(label=u'Correo Electrónico', max_length=50, widget= forms.TextInput(attrs={'class': ''}),
                                 required=True)
    password = forms.CharField(label=u'Contraseña', widget=forms.PasswordInput(render_value=False, attrs={'class': ''}),
                                required=True)
    cpassword = forms.CharField(label=u'Verificar contraseña',
                                widget=forms.PasswordInput(render_value=False, attrs={'class': ''}),
                                required=True)

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)

    def clean(self):
        if Usuario.objects.filter(email=self.cleaned_data['email']).count():
            raise forms.ValidationError(u'El correo no se encuentra disponible')

        if Usuario.objects.filter(username=self.cleaned_data['nickname']).count():
            raise forms.ValidationError(u'El nombre de usuario no se encuentra disponible')
        else:
            if not Usuario.objects.nombre_usuario_valido(self.cleaned_data['nickname']):
                raise forms.ValidationError(u'El nombre de usuario introducido no tiene un formato correcto')

        if len(self.cleaned_data['password']) < 6 or len(self.cleaned_data['password']) > 15 or re.match('^[\w\.-]+$',self.cleaned_data['password']) is None:
            raise forms.ValidationError(u'Las contrasenas deben tener entre 6 y 15 caracteres. Intentelo nuevamente')

        if self.cleaned_data['password'] != self.cleaned_data['cpassword']:
            raise forms.ValidationError(u'Las contrasenas son distintas. Intentelo nuevamente')
        return self.cleaned_data


class monedaForm(ModelForm):
    class Meta:
        model = Moneda
        fields = ['divisa']

    def __init__(self, *args, **kwargs):
        super(monedaForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

class capitalesForm(ModelForm):
    class Meta:
        model = Capitales
        fields = ['cantidad','divisa']

    def __init__(self, *args, **kwargs):
        super(capitalesForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

class transaccionesForm(ModelForm):
    class Meta:
        model = Transaccion
        fields = ['tipo','monto','divisa','descripcion','cliente']

    def __init__(self, *args, **kwargs):
        super(transaccionesForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class clienteForm(ModelForm):
    class Meta:
        model = Cliente
        fields = ['Nombre','Apellido','Cedula','Direccion','Telefono','Correo','Pseudonimo_MercadoLibre']

    def __init__(self, *args, **kwargs):
        super(clienteForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class articuloForm(ModelForm):
    class Meta:
        model = Articulo
        fields = ['modelo','costo','peso','precio','categoria','marca','cantidad']

    def __init__(self, *args, **kwargs):
        super(articuloForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class pedidoForm(ModelForm):
    class Meta:
        model = Pedido
        fields = ['fechaCompra','fechaRecepcion','costoReal','fechaEsRecepcion','divisa','costoTotal','pesoTotal','costoEstimado','costoReal']

    def __init__(self, *args, **kwargs):
        super(pedidoForm, self).__init__(*args, **kwargs)
        self.fields['costoTotal'].widget = TextInput(attrs={'readonly': True})
        self.fields['pesoTotal'].widget = TextInput(attrs={'readonly': True})
        self.fields['costoEstimado'].widget = TextInput(attrs={'readonly': True})
        self.fields['fechaEsRecepcion'].widget = TextInput(attrs={'readonly': True})

        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

        self.fields['fechaCompra'].widget = TextInput(attrs={'class': 'form-control datepicker'})
        self.fields['fechaRecepcion'].widget = TextInput(attrs={'class': 'form-control datepicker'})


class pedidoLineaForm(ModelForm):
    class Meta:
        model = LineaPedido
        fields = ['articulo','cantidad']

    def __init__(self, *args, **kwargs):
        super(pedidoLineaForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

class transaccionVentaForm(ModelForm):
    class Meta:
        model = Transaccion
        fields = ['divisa','cliente']

    def __init__(self, *args, **kwargs):
        super(transaccionVentaForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

class transaccionCompraDivisa(ModelForm):
    class Meta:
        model = Transaccion
        fields = ['monto']

    def __init__(self, *args, **kwargs):
        super(transaccionCompraDivisa, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

class lineaDivisaForm(ModelForm):
    class Meta:
        model = LineaDivisa
        fields = ['divisa_compra','cantidad']

    def __init__(self, *args, **kwargs):
        super(lineaDivisaForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class lineaVentaForm(ModelForm):
    class Meta:
        model = LineaVenta
        fields = ['articulo','cantidad']

    def __init__(self, *args, **kwargs):
        super(lineaVentaForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class categoriaForm(ModelForm):
    class Meta:
        model = Categoria
        fields = ['titulo','padre']

    def __init__(self, *args, **kwargs):
        super(categoriaForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'



class marcaForm(ModelForm):
    class Meta:
        model = Marca
        fields = ['titulo']

    def __init__(self, *args, **kwargs):
        super(marcaForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

class caracteristicaForm(ModelForm):
    class Meta:
        model = Caracteristicas
        fields = ['titulo','descripcion']

    def __init__(self, *args, **kwargs):
        super(caracteristicaForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'