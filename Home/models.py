from __future__ import division
# -*- coding: utf-8 -*-
from django.contrib import admin
from django.db import models
from django.utils import timezone
from datetime import date,timedelta
from django.contrib.auth.models import User
import re

DIVISAS = (
    ('BF','Bolivares en Efectivo') ,
    ('BE','Bolivares Electronicos') ,
    ('DF','Dolares en Efectivo') ,
    ('DE','Dolares Electronicos') ,
    ('EF','Eutos en Efectivo') ,
    ('EE','Euros Electronicos') ,
    ('DG','Dolares en GiftCard') ,
)

class Moneda(models.Model):
    divisa = models.CharField(max_length=50,choices=DIVISAS,default='BE')
    costo_promedio = models.FloatField(default=0.0,null=False,blank=True)

    def __str__(self):
        return self.get_divisa_display()

    class Meta:
        verbose_name = "moneda"
        db_table = "moneda"

admin.site.register(Moneda)

class Art_ML(models.Model):
    warranty = models.CharField(max_length=500, null=True)
    original_price = models.FloatField(default=0.0,null=True,blank=True)
    subtitle = models.CharField(max_length=150, null=True)
    geolocation = models.CharField(max_length=150, null=True)
    listing_source = models.CharField(max_length=150, null=True)
    site_id = models.CharField(max_length=150, null=True)
    buying_mode = models.CharField(max_length=150, null=True)
    seller_custom_field = models.CharField(max_length=150, null=True)
    currency_id = models.CharField(max_length=150, null=True)
    descriptions = models.CharField(max_length=150, null=True)
    automatic_relist = models.CharField(max_length=150, null=True)
    last_updated = models.CharField(max_length=150, null=True)
    ML_id = models.CharField(max_length=150, null=True)
    secure_thumbnail = models.CharField(max_length=150, null=True)
    title = models.CharField(max_length=150, null=True)
    pictures = models.CharField(max_length=2000, null=True)
    coverage_areas = models.CharField(max_length=150, null=True)
    stop_time = models.CharField(max_length=150, null=True)
    price = models.FloatField(default=0.0,null=True,blank=True)
    seller_contact = models.CharField(max_length=150, null=True)
    location = models.CharField(max_length=150, null=True)
    status = models.CharField(max_length=150, null=True)
    parent_item_id = models.CharField(max_length=150, null=True)
    tags = models.CharField(max_length=150, null=True)
    differential_pricing = models.CharField(max_length=150, null=True)
    start_time = models.CharField(max_length=150, null=True)
    permalink = models.CharField(max_length=150, null=True)
    official_store_id = models.CharField(max_length=150, null=True)
    date_created = models.CharField(max_length=150, null=True)
    accepts_mercadopago = models.CharField(max_length=150, null=True)
    available_quantity = models.IntegerField(default=0,null=True,blank=True)
    condition = models.CharField(max_length=150, null=True)
    sub_status = models.CharField(max_length=150, null=True)
    seller_id = models.IntegerField(default=0,null=True,blank=True)
    catalog_product_id = models.CharField(max_length=150, null=True)
    seller_address = models.CharField(max_length=750, null=True)
    video_id = models.CharField(max_length=150, null=True)
    variations = models.CharField(max_length=150, null=True)
    non_mercado_pago_payment_methods = models.CharField(max_length=150, null=True)
    shipping = models.CharField(max_length=150, null=True)
    thumbnail = models.CharField(max_length=150, null=True)
    end_time = models.CharField(max_length=150, null=True)
    sold_quantity = models.IntegerField(default=0,null=True,blank=True)
    listing_type_id = models.CharField(max_length=150, null=True)
    attributes = models.CharField(max_length=150, null=True)
    category_id = models.CharField(max_length=150, null=True)
    initial_quantity = models.IntegerField(default=0,null=True,blank=True)
    deal_ids = models.CharField(max_length=150, null=True)
    base_price = models.FloatField(default=0.0,null=True,blank=True)
    fecha = models.DateTimeField(default=timezone.now, null=True, blank=True)


    def parse(self,json):
        self.warranty=json["warranty"]
        self.original_price=json["original_price"]
        self.subtitle=json["subtitle"]
        self.geolocation=json["geolocation"]
        self.listing_source=json["listing_source"]
        self.site_id=json["site_id"]
        self.buying_mode=json["buying_mode"]
        self.seller_custom_field=json["seller_custom_field"]
        self.currency_id=json["currency_id"]
        self.descriptions=json["descriptions"]
        self.automatic_relist=json["automatic_relist"]
        self.last_updated=json["last_updated"]
        self.ML_id=json["id"]
        self.secure_thumbnail=json["secure_thumbnail"]
        self.title=json["title"]
        self.pictures=json["pictures"]
        self.coverage_areas=json["coverage_areas"]
        self.stop_time=json["stop_time"]
        self.price=json["price"]
        self.seller_contact=json["seller_contact"]
        self.location=json["location"]
        self.status=json["status"]
        self.parent_item_id=json["parent_item_id"]
        self.tags=json["tags"]
        self.differential_pricing=json["differential_pricing"]
        self.start_time=json["start_time"]
        self.permalink=json["permalink"]
        self.official_store_id=json["official_store_id"]
        self.date_created=json["date_created"]
        self.accepts_mercadopago=json["accepts_mercadopago"]
        self.available_quantity=json["available_quantity"]
        self.condition=json["condition"]
        self.sub_status=json["sub_status"]
        self.seller_id=json["seller_id"]
        self.catalog_product_id=json["catalog_product_id"]
        self.seller_address=json["seller_address"]
        self.video_id=json["video_id"]
        self.variations=json["variations"]
        self.non_mercado_pago_payment_methods=json["non_mercado_pago_payment_methods"]
        self.shipping=json["shipping"]
        self.thumbnail=json["thumbnail"]
        self.end_time=json["end_time"]
        self.sold_quantity=json["sold_quantity"]
        self.listing_type_id=json["listing_type_id"]
        self.attributes=json["attributes"]
        self.category_id=json["category_id"]
        self.initial_quantity=json["initial_quantity"]
        self.deal_ids=json["deal_ids"]
        self.base_price=json["base_price"]

    def __str__(self):
        return self.title

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in Art_ML._meta.fields]

admin.site.register(Art_ML)



#def parse_all(items):
#    params = {}
#    params["access_token"] = meli.access_token
#    articulos = []
#    for i in items["results"]:
#        response = meli.get("/items/"+i,params)
#        temp = response.json()
#        temp_art = Art_ML()
#        temp_art.parse(temp)
#        articulos.append(temp_art)
#    return articulos

#def parse_and_save(items):
#    params = {}
#    params["access_token"] = meli.access_token
#    articulos = []
#    for i in items["results"]:
#        response = meli.get("/items/"+i,params)
#        temp = response.json()
#        try:
#            temp_art = Art_ML.objects.get(ML_id=i)
#        except:
#            temp_art = Art_ML()
#        temp_art.parse(temp)
#        temp_art.save()
#        articulos.append(temp_art)
#    return articulos


class Cliente(models.Model):
    Nombre = models.CharField(max_length=50)
    Apellido = models.CharField(max_length=50)
    Cedula = models.CharField(max_length=50)
    Direccion = models.CharField(max_length=150)
    Telefono = models.CharField(max_length=50)
    Correo = models.CharField(max_length=50)
    Pseudonimo_MercadoLibre = models.CharField(max_length=50)
    fecha_creacion = models.DateField(auto_now=True,null=False,blank=True)

    def __str__(self):
        return self.Apellido + " " + self.Nombre

    class Meta:
        verbose_name = "cliente"
        db_table = "cliente"

admin.site.register(Cliente)




class Categoria(models.Model):
    titulo = models.CharField(max_length=150, db_index=True)
    padre = models.ForeignKey('self', null=True, blank=True, related_name="Hijo")
    hoja = models.BooleanField(default=True)

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = "Categoria"
        db_table = "Categoria"

    def to_dict(self):
        data = {}
        data["titulo"] = self.titulo
        if(self.padre):
            data["padre"] = self.padre.titulo
        data["hoja"] = self.hoja
        return data

admin.site.register(Categoria)

class Marca(models.Model):
    titulo = models.CharField(max_length=150, db_index=True)

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = "Marca"
        db_table = "Marca"

admin.site.register(Marca)

class Articulo(models.Model):
    modelo = models.CharField(max_length=150, db_index=True)
    costo = models.FloatField(default=0.0,null=True,blank=True)
    costoEnvio = models.FloatField(default=0.0,null=True,blank=True)
    costoTotal = models.FloatField(default=0.0,null=True,blank=True)
    peso = models.FloatField(default=0.0,null=True,blank=True)
    pesoVolumetrico = models.FloatField(default=0.0,null=True,blank=True)
    precio = models.FloatField(default=0.0,null=True,blank=True)
    categoria = models.ForeignKey(Categoria,null=True,blank=True)
    marca = models.ForeignKey(Marca,null=True,blank=True)
    cantidad = models.IntegerField(default=0,null=True,blank=True)
    umbral_compra = models.IntegerField(default=0,null=True,blank=True)

    def __str__(self):
        name =""+ self.categoria.titulo + " " + self.marca.titulo + " " + self.modelo
        return name

    class Meta:
        verbose_name = "articulo"
        db_table = "articulo"

admin.site.register(Articulo)


def ArticuloImagenesPath(instance, filename):
    folder = 'img_articulos/' + instance.articulo.__str__() + '/' + filename
    return folder


class Caracteristicas(models.Model):
    titulo = models.CharField(max_length=150, db_index=True)
    descripcion = models.CharField(max_length=200)
    articulo = models.ForeignKey(Articulo,null=False, related_name="caracteristicas")

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = "caracteristicas"
        db_table = "caracteristicas"

admin.site.register(Caracteristicas)



class Pedido(models.Model):
    pagado = models.BooleanField(default=False,null=False,blank=True)
    divisa = models.CharField(max_length=50,choices=DIVISAS,default='DE')
    costoTotal = models.FloatField(default=0.0,null=False,blank=True)
    fechaCompra = models.DateField(default=date.today,null=False,blank=False)
    fechaEsRecepcion = models.DateField(default=date.today,null=False,blank=True)
    fechaRecepcion = models.DateField(default=date.today,null=False,blank=True)
    pesoTotal = models.FloatField(default=0.0,null=False,blank=True)
    costoEstimado = models.FloatField(default=0.0,null=False,blank=True)
    costoReal = models.FloatField(default=0.0,null=False,blank=True)

    def __str__(self):
        string = "Pedido del " + str(self.fechaCompra)
        return string

    class Meta:
        verbose_name = "pedido"
        db_table = "pedido"

    def CalcularFechaRecepcion(self):
        try:
            pedidos = Pedido.objects.all()
            total = 0
            for a in pedidos:
                estTemp = a.fechaRecepcion - a.fechaCompra
                total += estTemp.days

            total = (total / pedidos.__len__())
            self.fechaEsRecepcion = self.fechaCompra + timedelta(days=total)
        except:
            return False

    def CalcularCostoTotal(self):
        lineas = LineaPedido.objects.filter(pedido = self)
        cstTemp = 0
        for l in lineas:
            cstTemp += l.articulo.costo * l.cantidad
        self.costoTotal = cstTemp
        self.save()

    def CalcularCostoEstimado(self):
        try:
            pedidos = Pedido.objects.all()
            total = 0
            for a in pedidos:
                if(a.pesoTotal > 0):
                    estTemp = a.costoReal / a.pesoTotal
                else:
                    estTemp = 0
                total += estTemp

            total = (total / pedidos.__len__())
            self.costoEstimado = (int((self.pesoTotal * total )*100))/100
        except:
            return False

    def ActualizarPeso(self):
        lineas = LineaPedido.objects.filter(pedido = self)
        pesoTotal = 0
        for l in lineas:
            pesoTotal += l.articulo.peso * l.cantidad
        self.pesoTotal = pesoTotal
        self.CalcularCostoEstimado()
        self.CalcularCostoTotal()
        self.save()

admin.site.register(Pedido)

class LineaPedido(models.Model):
    articulo = models.ForeignKey(Articulo,null=False, related_name="pedido")
    pedido = models.ForeignKey(Pedido,null=False, related_name="lineas")
    cantidad = models.IntegerField(default=0,null=False,blank=False)

    def __str__(self):
        string = "linea del pedido del " + str(self.pedido.fechaCompra)
        return string

    class Meta:
        verbose_name = "lineapedido"
        db_table = "lineapedido"

admin.site.register(LineaPedido)


class Transaccion(models.Model):
    fecha = models.DateField(auto_now=True,null=False,blank=True)
    TIPO = (
        ('I','Ingreso') ,
        ('E','Egreso') ,
        ('O','Otro')
    )
    tipo = models.CharField(max_length=8,choices=TIPO)
    monto = models.FloatField(default=0.0)
    impuesto = models.FloatField(default=0.0)
    divisa = models.CharField(max_length=50,choices=DIVISAS,default='BE')
    cliente = models.ForeignKey(Cliente ,blank=True,null=True)
    descripcion = models.CharField(max_length=150,blank=True,default="")
    registrada = models.BooleanField(default=False)

    def __str__(self):
        return self.descripcion

    def Facturar(self):
        try:
            if(not self.registrada):
                list = LineaVenta.objects.filter(transaccion = self)
                for l in list:
                    if((l.articulo.cantidad - l.cantidad) < 0):
                        msg = "No Posee suficiente " + l.articulo.__str__()
                        return msg

                for l in list:
                        l.articulo.cantidad -= l.cantidad
                        l.articulo.save()

                self.tipo = 'I'
                if(self.cliente == None):
                    msg = "Debe Agregar un cliente"
                    return msg

                self.descripcion = "Venta a " + self.cliente.__str__() + " " + str(self.fecha)
                self.registrada = True
                self.save()
                ActualizarCapital(self.tipo,self.monto,self.divisa)
                NuevaGanancia(self)
            return True
        except:
            msg = "No puede facturar esta Transaccion"
            return msg


    class Meta:
        verbose_name = "transaccion"
        db_table = "transaccion"

admin.site.register(Transaccion)


class LineaVenta(models.Model):
    articulo = models.ForeignKey(Articulo,null=False, related_name="articuloLineaVenta")
    transaccion = models.ForeignKey(Transaccion,null=False, related_name="lineaVenta")
    cantidad = models.IntegerField(default=0,null=False,blank=False)

    def __str__(self):
        string = "linea del pedido del " + str(self.transaccion.fecha)
        return string

    class Meta:
        verbose_name = "lineaventa"
        db_table = "lineaventa"

admin.site.register(LineaVenta)


class LineaDivisa(models.Model):
    divisa_compra = models.CharField(max_length=50,choices=DIVISAS,default='DE')
    transaccion = models.ForeignKey(Transaccion,null=False, related_name="lineaDivisa")
    cantidad = models.IntegerField(default=0,null=False,blank=False)

    def __str__(self):
        string = "linea de la Compra del " + str(self.transaccion.fecha)
        return string

    class Meta:
        verbose_name = "lineadivisa"
        db_table = "lineadivisa"

admin.site.register(LineaDivisa)


class Ganancias(models.Model):
    transaccion = models.OneToOneField(Transaccion,null=False, related_name="Ganancia")
    fecha_creacion = models.DateField(auto_now=True,null=False,blank=True)
    gananciaTT = models.FloatField(default=0.0,null=False,blank=True)
    gananciaDE = models.FloatField(default=0.0,null=False,blank=True)
    gananciaJagu = models.FloatField(default=0.0,null=False,blank=True)

    class Meta:
        verbose_name = "ganancias"
        db_table = "ganancias"

admin.site.register(Ganancias)

def GananciasDelMes():
    year = timezone.now().year
    month = timezone.now().month
    list =  Ganancias.objects.filter(fecha_creacion__year=year,
                                    fecha_creacion__month=month)
    ganancias = 0.0
    for g in list:
        ganancias +=g.gananciaTT
    return ganancias

def GananciasTipoMes(tipo):
    year = timezone.now().year
    month = timezone.now().month
    try:
        list =  Ganancias.objects.filter(fecha_creacion__year=year,
                                        fecha_creacion__month=month)
        ganancias = 0.0
        if(tipo == 'TT'):
            for g in list:
                ganancias +=g.gananciaTT
        elif(tipo == 'DE'):
            for g in list:
                ganancias +=g.gananciaDE
        elif(tipo == 'Jagu'):
            for g in list:
                ganancias +=g.gananciaJagu

        return ganancias
    except:
        return False

def NuevaGanancia(transaccion):
    ganancia = Ganancias()
    ganancia.transaccion = transaccion
    try:
        ganancias = GananciasTipoMes('DE')
        list = LineaVenta.objects.filter(transaccion = transaccion)
        total = transaccion.monto
        for l in list:
            total -= l.articulo.costoTotal*l.cantidad

        ganancia.gananciaTT = total

        div = 2
        if(ganancias > 4000):
            div = (2 * int(ganancias/4000))

        ganancia.gananciaDE = total/div
        ganancia.gananciaJagu = total/div
        ganancia.save()
        return True
    except:
        return False

class Capitales(models.Model):
    fecha = models.DateTimeField(auto_now=True,null=False,blank=True)
    cantidad = models.FloatField(default=0.0)
    divisa = models.CharField(max_length=50,choices=DIVISAS,default='BE')
    costo_promedio = models.FloatField(default=0.0,null=False,blank=True)

    class Meta:
        verbose_name = "capitales"
        db_table = "capitales"

admin.site.register(Capitales)


def ActualizarCapital(tipo,monto,divisa):
    NuevoCapital = Capitales()
    try:
        capital = Capitales.objects.filter(divisa=divisa).order_by('-fecha')[0]
        NuevoCapital.divisa = capital.divisa
        NuevoCapital.cantidad = capital.cantidad
    except:
        NuevoCapital.divisa = divisa
        NuevoCapital.cantidad = 0

    if(tipo == 'I'):
        NuevoCapital.cantidad +=monto
    elif(tipo == 'E'):
        NuevoCapital.cantidad -=monto
    NuevoCapital.save()

def IntercambioCapital(montoPago,divisaPago,monto,divisa):
    try:
        NuevoCapitalPago = Capitales()
        NuevoCapital = Capitales()
        capitalPago = Capitales.objects.filter(divisa=divisaPago).order_by('-fecha')[0]
        NuevoCapitalPago.cantidad = capitalPago.cantidad - montoPago
        NuevoCapitalPago.divisa = divisaPago
        NuevoCapitalPago.save()
        try:
            capital = Capitales.objects.filter(divisa=divisa).order_by('-fecha')[0]
            ca = capital.cantidad * capital.costo_promedio
            cb = montoPago
            NuevoCapital.costo_promedio = (ca + cb)/(monto + capital.cantidad)
            NuevoCapital.cantidad = capital.cantidad + monto
            NuevoCapital.divisa = divisa
        except:
            NuevoCapital.divisa = divisa
            NuevoCapital.cantidad = monto
            NuevoCapital.costo_promedio =  montoPago / monto
        NuevoCapital.save()
    except:
        return False

def ObtenerCapital(divisa):
    return Capitales.objects.filter(divisa=divisa).order_by('-fecha')[0]


class UsuarioManager(models.Manager):
    """
    Clase utilizada para realizar acciones de manera general sobre la clase usuario
    """
    def crear_usuario(self, username=None, email=None, password=None, first_name=None, last_name=None):
        """
        Metodo para crear un usuario nuevo en el sistema, segpun el tipo de usuario que este sea.
        """
        email = email.lower()
        first_name = "%s%s" % (first_name.upper()[:1], first_name.lower()[1:])
        last_name = "%s%s" % (last_name.upper()[:1], last_name.lower()[1:])
        usuario = Superadmin(username=username, email=email, first_name=first_name, last_name=last_name)
        usuario.set_password(password)
        usuario.save()
        return usuario

    def nombre_usuario_valido(self,nombre):
        if re.match('^[a-zA-Z0-9\.-_]+$', nombre) is not None:
            return True
        return False


class Usuario(User):
    objects = UsuarioManager()

    def to_dict(self):
        retorno = {
            'id': self.id,
            'username': self.username.encode('utf8'),
            'nombre': "%s %s" % (self.first_name.encode('utf8'), self.last_name.encode('utf8')),
            'email': self.email,
            'first_name' : self.first_name.encode('utf8'),
            'last_name' : self.last_name.encode('utf8')
        }

        if self.fecha_nacimiento:
            tmp = str(self.fecha_nacimiento).split("-")
            retorno['fecnac'] = "%s/%s/%s" % (tmp[2], tmp[1], tmp[0])
        return retorno

    class Meta:
        verbose_name = "usuario"
        db_table = "usuario"
admin.site.register(Usuario)

class Superadmin(Usuario):
    class Meta:
        verbose_name = "superadmin"
        db_table = "superadmin"
admin.site.register(Superadmin)


class Visitante(models.Model):
    nombre = models.CharField(max_length=50, null=False)
    email = models.CharField(max_length=50, null=False,db_index=True,unique=True)
    fecha = models.DateTimeField(default=timezone.now, null=False, blank=True)
admin.site.register(Visitante)















