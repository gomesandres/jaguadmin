# -*- coding: utf-8 -*-
from django.conf import settings
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.encoders import encode_base64
import os
import smtplib

class SendMail():
    '''
    Clase para envio de emails usando sendmail
    '''
    def __init__(self, *args, **kwargs):
        self.msg = None
        self.attachment = None


    def buildSendEMail(self,to,content,attach=None,mailfrom=None,mailsubject=None, upload_attach=None):
        '''
        Envia email de la(s) giftcard(s)
        '''

        try:
            self.msg = MIMEMultipart('mixed')
            self.msg['From'] = settings.SENDER
            self.msg['To'] = to
            self.msg['Subject'] = "Notificacion de Yanes.com" if mailsubject is None else mailsubject

            self.msg.attach( MIMEText(content,'html'))

            if attach is not None:
                self.attachment = MIMEBase('application', "octet-stream")
                self.attachment.set_payload( open(attach,"rb").read() )
                encode_base64(self.attachment)
                self.attachment.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(attach))
                self.msg.attach(self.attachment)

            if upload_attach is not None:
                self.attachment = MIMEBase('application', "octet-stream")
                self.attachment.set_payload(upload_attach.read())
                encode_base64(self.attachment)
                self.attachment.add_header('Content-Disposition', 'attachment; filename="%s"' % upload_attach)
                self.msg.attach(self.attachment)

            server = smtplib.SMTP('smtp.gmail.com:587')
            server.starttls()
            server.login(settings.SENDER,settings.SENDER_PASS)
            server.set_debuglevel(1)
            server.sendmail(settings.SENDER, to, self.msg.as_string())
            server.close()
            return True
        except:
            return False
