# -*- coding: utf-8 -*-
from django.db.models.fields.files import *
import types
import math
import shutil
        
class ExtendedImageFieldFile(ImageFieldFile):
    def __init__(self, *args, **kwargs):
        super(ExtendedImageFieldFile, self).__init__(*args, **kwargs)
        
        if self.name:
            path, name = os.path.split(self.name)
            if self.field.resize and self.field.keep_original:
                setattr(self, 'ori', ImageFieldFile(instance=None, field=ImageField(), name='%s/ori/%s' % (path, name)))
            if self.field.thumb_size:
                setattr(self, 'thumb', self.create_aditional(size=self.field.thumb_size))
            if self.field.fixed_size:
                setattr(self, 'fixed', self.create_aditional(size=self.field.fixed_size))
    
    def create_aditional(self, size):
        new_path, ext = self.name.rsplit('.', 1)
        width, height = size
        new_name = '%s_%sx%s.%s' % (new_path, width, height, ext)
        return ImageFieldFile(instance=None, field=ImageField(), name=new_name)

    def save(self, name, content, save=True):
        super(ExtendedImageFieldFile, self).save(name, content, save)
        
        # Crea el thumbnail de la imagen
        if self.field.thumb_size:
            original = self.open_rgb_image()
            original.thumbnail(self.field.thumb_size, Image.ANTIALIAS)
            thumb_path = self.__create_path(self.field.thumb_size)
            try:
                original.save(thumb_path)
            except IOError:
                self.delete()
                print 'Can\'t create image thumbnail.'
        
        #Primero calcula la nueva altura. 
        #Si la nueva altura es menor al tamaño que hay que cortar buscar el nuevo ancho.
        #Despues hace un thumbnail de la imagen y por ultimo corta la imagen para que quede del tamaño exacto
        if self.field.fixed_size:
            original = self.open_rgb_image()
            width, height = original.size
            crop_width, crop_height = self.field.fixed_size
            
            new_width = crop_width
            new_height = int(float(height) / float(width) * crop_width)
            top = int((new_height-crop_height) / 2)
            bottom = crop_height+top
            crop = (0, top, crop_width, bottom)
            if new_height < crop_height:
                new_width = float(width) / float(height) * crop_height
                new_height = crop_height
                left = int((new_width-crop_width) / 2)
                right = int(crop_width+left)
                crop = (left, 0, right, crop_height)
            
            crop_path = self.__create_path(self.field.fixed_size)
            try:
                original.thumbnail((int(math.ceil(new_width)), int(math.ceil(new_height))), Image.ANTIALIAS)
                cropped = original.crop(crop)
                cropped.save(crop_path)
            except IOError:
                self.delete()
                print 'Can\'t create image cropped.'
        
        # Si hay que resizear la image, se hace un resize de la imagen ya subida
        # Lo hago al final para crear los thumbs y los crops con la imagen original y no perder calidad.
        if self.field.resize:
            original = self.open_rgb_image()                
            if self.field.keep_original:
                path, name = os.path.split(self.path)
                ori_path = '%s/%s' % (path, 'ori')
                if not os.path.exists(ori_path):
                    os.mkdir(ori_path)
                shutil.copy(self.path, '%s/%s' % (ori_path, name))
            try:
                original.thumbnail(self.field.resize, Image.ANTIALIAS)
                original.save(self.path)
            except IOError:
                self.delete()
                print 'Can\'t resize image.'
    
    def open_rgb_image(self):
        original = Image.open(self.path)
        if original.mode not in ('L', 'RGB'):
            original = original.convert('RGB')
        return original
    
    def __create_path(self, size):
        width, height = size
        new_path, ext = self.path.rsplit('.', 1)
        return '%s_%sx%s.%s' % (new_path, width, height, ext)
    
    def delete(self, save=True):
        try:
            if self.field.resize and self.field.keep_original:
                path, name = os.path.split(self.path)
                self.storage.delete('%s/%s/%s' % (path, 'ori', name))
            if self.field.thumb_size:
                self.storage.delete(self.__create_path(self.field.thumb_size))
            if self.field.fixed_size:
                self.storage.delete(self.__create_path(self.field.fixed_size))
        except:
            pass
        super(ExtendedImageFieldFile, self).delete(save)
        
class ExtendedImageField(ImageField):
    attr_class = ExtendedImageFieldFile
    def __init__(self, verbose_name=None, name=None, width_field=None, height_field=None,
                 resize=None, thumb_size=None, fixed_size=None, keep_original=False,
                 **kwargs):
        self.verbose_name = verbose_name
        self.name = name
        self.width_field = width_field
        self.height_field = height_field
        self.keep_original = keep_original

        if thumb_size and not isinstance(thumb_size, types.TupleType):
            raise TypeError('thumb_size argument must be a tuple')
        if resize and not isinstance(resize, types.TupleType):
            raise TypeError('new_size argument must be a tuple')
        if fixed_size and not isinstance(fixed_size, types.TupleType):
            raise TypeError('new_size argument must be a tuple')

        self.thumb_size = thumb_size
        self.resize = resize
        self.fixed_size = fixed_size
        super(ImageField, self).__init__(**kwargs)

